package models;

public class Time {
    private int hour = 5;
    private int minute = 30;
    private int second = 0;

    // khởi tạo với 3 tham số
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void setTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    // tiến lên 1 giây
    public Time nextSecond(int second) {
        // set thời gian lên 1 giây
        this.setSecond(this.getSecond() + 1);
        return this;
    }

    // giảm đi 1 giây
    public Time previousSecond( int second) {
        // set thời gian xuống 1 giây
        this.setSecond(this.getSecond() - 1);
        return this;
    }

    @Override
    public String toString() {
        return "Time [hour=" + hour + ", minute=" + minute + ", second=" + second + "]";
    }

}
